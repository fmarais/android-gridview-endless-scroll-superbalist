package com.marais.android_gridview_endless_scroll_superbalist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.marais.android_gridview_endless_scroll_superbalist.adapters.PhotosAdapter;
import com.marais.android_gridview_endless_scroll_superbalist.api.Service;
import com.marais.android_gridview_endless_scroll_superbalist.api.ServiceBuilder;
import com.marais.android_gridview_endless_scroll_superbalist.api.models.PhotosResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";
    private Service apiService;
    private PhotosAdapter photosAdapter;
    // 1 index not 0 index
    private int currentPage = 2;
    private LinearLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loading = (LinearLayout) findViewById(R.id.loading_layout);

        initAdapter();
        initApi();
        apiLoadPhotos(1);
        apiLoadPhotos(2);
    }

    private void initAdapter() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.photos_layout);
        recyclerView.setLayoutManager(new GridLayoutManager(
                this,
                getResources().getInteger(R.integer.grid_items)));

        photosAdapter = new PhotosAdapter(
                this,
                recyclerView,
                new PhotosAdapter.AdapterInterface() {
                    @Override
                    public void loadMore() {
                        apiLoadPhotos(currentPage + 1);
                    }
                });
        recyclerView.setAdapter(photosAdapter);
    }

    private void initApi() {
        apiService = ServiceBuilder.build(this);
    }

    private void apiLoadPhotos(int page) {
        loadStart();
        currentPage = page;

        apiService.getPhotos(
                page,
                getString(R.string.consumer_key)).enqueue(new Callback<PhotosResponse>() {

            @Override
            public void onResponse(
                    Call<PhotosResponse> call,
                    Response<PhotosResponse> response) {

                Log.i(TAG, "onResponse()");

                // refresh adapter with new data
                if (response != null
                        && response.body() != null
                        && response.body().getPhotos() != null) {

                    photosAdapter.setData(response.body().getPhotos());
                    photosAdapter.notifyDataSetChanged();
                }

                loadComplete();
            }

            @Override
            public void onFailure(Call<PhotosResponse> call, Throwable t) {
                Log.i(TAG, "onFailure()");
                loadComplete();
            }
        });
    }

    private void loadComplete() {
        if (loading != null) {
            loading.setVisibility(View.GONE);
        }
    }

    private void loadStart() {
        if (loading != null) {
            loading.setVisibility(View.VISIBLE);
        }
    }
}