package com.marais.android_gridview_endless_scroll_superbalist.api.models;

public class Photo {
    private String image_url;

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String image_url) {
        this.image_url = image_url;
    }
}
