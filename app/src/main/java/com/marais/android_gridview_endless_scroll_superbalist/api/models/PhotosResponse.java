package com.marais.android_gridview_endless_scroll_superbalist.api.models;

import java.util.ArrayList;

public class PhotosResponse {
    private int current_page;
    private int total_pages;
    private int total_items;
    private ArrayList<Photo> photos;

    public int getCurrentPage() {
        return current_page;
    }

    public void setCurrentPage(int current_page) {
        this.current_page = current_page;
    }

    public int getTotalPages() {
        return total_pages;
    }

    public void setTotalPages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotalItems() {
        return total_items;
    }

    public void setTotalItems(int total_items) {
        this.total_items = total_items;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
}