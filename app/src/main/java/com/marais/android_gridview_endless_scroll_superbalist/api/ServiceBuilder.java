package com.marais.android_gridview_endless_scroll_superbalist.api;

import android.content.Context;

import com.marais.android_gridview_endless_scroll_superbalist.R;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceBuilder {
    public static Service build(Context context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.endpoint_base))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(Service.class);
    }
}

