package com.marais.android_gridview_endless_scroll_superbalist.api;

import com.marais.android_gridview_endless_scroll_superbalist.api.models.PhotosResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service {
    @GET("/v1/photos")
    Call<PhotosResponse> getPhotos(
            @Query("page") int page,
            @Query("consumer_key") String consumer_key
    );
}