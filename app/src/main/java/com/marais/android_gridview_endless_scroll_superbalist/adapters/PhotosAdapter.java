package com.marais.android_gridview_endless_scroll_superbalist.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.marais.android_gridview_endless_scroll_superbalist.R;
import com.marais.android_gridview_endless_scroll_superbalist.api.models.Photo;

import java.util.ArrayList;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {
    private static final String TAG = "PhotosAdapter";
    private final Context context;
    private final AdapterInterface adapterInterface;
    private ArrayList<Photo> data = new ArrayList<>();
    private boolean loading;

    public PhotosAdapter(Context context,
                         RecyclerView recyclerView,
                         AdapterInterface adapterInterface) {

        this.context = context;
        this.adapterInterface = adapterInterface;
        endlessScroll(recyclerView);
    }

    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.photos_adapter_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PhotosAdapter.ViewHolder holder, int position) {
        // load image
        Glide.with(context)
                .load(data.get(position).getImageUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.loading.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.loading.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.photo);

        // load complete, listen for scroll
        if (position + 1 == getItemCount()) {
            loading = false;
        }
    }

    public int getItemCount() {
        if (data != null) {
            return data.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView photo;
        private LinearLayout loading;

        public ViewHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.photo);
            loading = itemView.findViewById(R.id.loading_layout);
        }
    }

    // custom methods
    public void setData(ArrayList<Photo> data) {
        if (data != null) {
            this.data.addAll(data);
            Log.i(TAG, "setData() size: " + this.data.size());
        }
    }

    private void endlessScroll(RecyclerView recyclerView) {
        final GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!loading
                        && layoutManager != null
                        && getItemCount()
                        == layoutManager.findLastVisibleItemPosition()
                        + 1) {

                    loading = true;
                    adapterInterface.loadMore();
                }
            }
        });
    }

    // used to inform calling activity that all items for this page has loaded
    // ========== interface ==========
    public interface AdapterInterface {
        void loadMore();
    }
}
